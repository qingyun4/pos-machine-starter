package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public Map<String,Integer> countBarcodes(List<String> barcodes){

        Map<String,Integer> countMap = new HashMap<>();
        for (String barcode : barcodes) {
            if(countMap.containsKey(barcode)){
                int quantity = countMap.get(barcode);
                countMap.put(barcode,quantity+1);
            }
            else{
                countMap.put(barcode, 1);
            }
        }
        return countMap;
    }

    public Item matchItem(String barcode){
        List<Item> items = ItemsLoader.loadAllItems();
        for (Item item : items) {
            if(item.getBarcode().equals(barcode))
                return item;
        }
        return null;
    }

    public List<ReceiptItem> makeReceiptItemList(List<String> barcodes){
        Map<String,Integer> countMap = countBarcodes(barcodes);
        List<ReceiptItem> receiptItemList = new ArrayList<>();
        for(Map.Entry<String, Integer> entry : countMap.entrySet()){
            String barcode = entry.getKey();
            Item item = matchItem(barcode);
            receiptItemList.add(makeReceiptItem(entry, item));
        }
        return receiptItemList;
    }

    public ReceiptItem makeReceiptItem(Map.Entry<String, Integer> entry, Item item){
        ReceiptItem receiptItem = new ReceiptItem(item.getName(),entry.getValue(),item.getPrice());
        return receiptItem;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItemList){
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItemList) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public Receipt makeReceipt(List<String> barcodes){
        List<ReceiptItem> receiptItemList = makeReceiptItemList(barcodes);
        int totalPrice = calculateTotalPrice(receiptItemList);
        Receipt receipt = new Receipt(receiptItemList, totalPrice);
        return receipt;
    }

    public List<String> generateItemReceipt(Receipt receipt){
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        List<String> outputItems = new ArrayList<>();
        String output = "";
        for (ReceiptItem receiptItem : receiptItems) {
            output = "Name: " + receiptItem.getName() + ", Quantity: " + receiptItem.getQuantity() + ", Unit price: " + receiptItem.getUnitPrice() + " (yuan), Subtotal: " + receiptItem.getSubTotal() +" (yuan)";
            outputItems.add(output);
        }
        return outputItems;
    }

    public String  generateReceipt(List<String> barcodes){
        Receipt receipt = makeReceipt(barcodes);
        List<String> outputItems = generateItemReceipt(receipt);
        String renderReceipt = "";
        for (String outputItem : outputItems) {
            renderReceipt += outputItem + "\n";
        }
        renderReceipt += "----------------------\n" + "Total: " +
                receipt.getTotalPrice() +
                " (yuan)\n";
        return renderReceipt;
    }

    public String printReceipt(List<String> barcodes) {
        String renderReceipt = generateReceipt(barcodes);
        String printReceipt = "";
        printReceipt = "***<store earning no money>Receipt***\n" +
                renderReceipt +
                "**********************";
        return printReceipt;
    }
}
